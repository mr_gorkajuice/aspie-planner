﻿namespace Aspie_Planner.Forms
{
    partial class WeekPlannerSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.timeSundayTo = new System.Windows.Forms.DateTimePicker();
            this.timeSundayFrom = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.timeSaturdayTo = new System.Windows.Forms.DateTimePicker();
            this.timeSaturdayFrom = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.timeFridayTo = new System.Windows.Forms.DateTimePicker();
            this.timeFridayFrom = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.timeThursdayTo = new System.Windows.Forms.DateTimePicker();
            this.timeThursdayFrom = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timeWednesdayTo = new System.Windows.Forms.DateTimePicker();
            this.timeWednesdayFrom = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.timeTuesdayTo = new System.Windows.Forms.DateTimePicker();
            this.timeTuesdayFrom = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timeMondayTo = new System.Windows.Forms.DateTimePicker();
            this.timeMondayFrom = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbHour = new System.Windows.Forms.RadioButton();
            this.rbHalfHour = new System.Windows.Forms.RadioButton();
            this.rbQuarter = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.timeSundayTo);
            this.groupBox1.Controls.Add(this.timeSundayFrom);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.timeSaturdayTo);
            this.groupBox1.Controls.Add(this.timeSaturdayFrom);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.timeFridayTo);
            this.groupBox1.Controls.Add(this.timeFridayFrom);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.timeThursdayTo);
            this.groupBox1.Controls.Add(this.timeThursdayFrom);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.timeWednesdayTo);
            this.groupBox1.Controls.Add(this.timeWednesdayFrom);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.timeTuesdayTo);
            this.groupBox1.Controls.Add(this.timeTuesdayFrom);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.timeMondayTo);
            this.groupBox1.Controls.Add(this.timeMondayFrom);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(211, 211);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tilgængelighed";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(121, 180);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "til";
            // 
            // timeSundayTo
            // 
            this.timeSundayTo.CustomFormat = "HH:mm";
            this.timeSundayTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeSundayTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeSundayTo.Location = new System.Drawing.Point(141, 178);
            this.timeSundayTo.Name = "timeSundayTo";
            this.timeSundayTo.ShowUpDown = true;
            this.timeSundayTo.Size = new System.Drawing.Size(55, 20);
            this.timeSundayTo.TabIndex = 26;
            // 
            // timeSundayFrom
            // 
            this.timeSundayFrom.CustomFormat = "HH:mm";
            this.timeSundayFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeSundayFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeSundayFrom.Location = new System.Drawing.Point(60, 178);
            this.timeSundayFrom.Name = "timeSundayFrom";
            this.timeSundayFrom.ShowUpDown = true;
            this.timeSundayFrom.Size = new System.Drawing.Size(55, 20);
            this.timeSundayFrom.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 180);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Søndag";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(121, 154);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "til";
            // 
            // timeSaturdayTo
            // 
            this.timeSaturdayTo.CustomFormat = "HH:mm";
            this.timeSaturdayTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeSaturdayTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeSaturdayTo.Location = new System.Drawing.Point(141, 152);
            this.timeSaturdayTo.Name = "timeSaturdayTo";
            this.timeSaturdayTo.ShowUpDown = true;
            this.timeSaturdayTo.Size = new System.Drawing.Size(55, 20);
            this.timeSaturdayTo.TabIndex = 22;
            // 
            // timeSaturdayFrom
            // 
            this.timeSaturdayFrom.CustomFormat = "HH:mm";
            this.timeSaturdayFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeSaturdayFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeSaturdayFrom.Location = new System.Drawing.Point(60, 152);
            this.timeSaturdayFrom.Name = "timeSaturdayFrom";
            this.timeSaturdayFrom.ShowUpDown = true;
            this.timeSaturdayFrom.Size = new System.Drawing.Size(55, 20);
            this.timeSaturdayFrom.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 154);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Lørdag";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(121, 128);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "til";
            // 
            // timeFridayTo
            // 
            this.timeFridayTo.CustomFormat = "HH:mm";
            this.timeFridayTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeFridayTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeFridayTo.Location = new System.Drawing.Point(141, 126);
            this.timeFridayTo.Name = "timeFridayTo";
            this.timeFridayTo.ShowUpDown = true;
            this.timeFridayTo.Size = new System.Drawing.Size(55, 20);
            this.timeFridayTo.TabIndex = 18;
            // 
            // timeFridayFrom
            // 
            this.timeFridayFrom.CustomFormat = "HH:mm";
            this.timeFridayFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeFridayFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeFridayFrom.Location = new System.Drawing.Point(60, 126);
            this.timeFridayFrom.Name = "timeFridayFrom";
            this.timeFridayFrom.ShowUpDown = true;
            this.timeFridayFrom.Size = new System.Drawing.Size(55, 20);
            this.timeFridayFrom.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Fredag";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(121, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "til";
            // 
            // timeThursdayTo
            // 
            this.timeThursdayTo.CustomFormat = "HH:mm";
            this.timeThursdayTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeThursdayTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeThursdayTo.Location = new System.Drawing.Point(141, 100);
            this.timeThursdayTo.Name = "timeThursdayTo";
            this.timeThursdayTo.ShowUpDown = true;
            this.timeThursdayTo.Size = new System.Drawing.Size(55, 20);
            this.timeThursdayTo.TabIndex = 14;
            // 
            // timeThursdayFrom
            // 
            this.timeThursdayFrom.CustomFormat = "HH:mm";
            this.timeThursdayFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeThursdayFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeThursdayFrom.Location = new System.Drawing.Point(60, 100);
            this.timeThursdayFrom.Name = "timeThursdayFrom";
            this.timeThursdayFrom.ShowUpDown = true;
            this.timeThursdayFrom.Size = new System.Drawing.Size(55, 20);
            this.timeThursdayFrom.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 102);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Torsdag";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(121, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "til";
            // 
            // timeWednesdayTo
            // 
            this.timeWednesdayTo.CustomFormat = "HH:mm";
            this.timeWednesdayTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeWednesdayTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeWednesdayTo.Location = new System.Drawing.Point(141, 74);
            this.timeWednesdayTo.Name = "timeWednesdayTo";
            this.timeWednesdayTo.ShowUpDown = true;
            this.timeWednesdayTo.Size = new System.Drawing.Size(55, 20);
            this.timeWednesdayTo.TabIndex = 10;
            // 
            // timeWednesdayFrom
            // 
            this.timeWednesdayFrom.CustomFormat = "HH:mm";
            this.timeWednesdayFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeWednesdayFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeWednesdayFrom.Location = new System.Drawing.Point(60, 74);
            this.timeWednesdayFrom.Name = "timeWednesdayFrom";
            this.timeWednesdayFrom.ShowUpDown = true;
            this.timeWednesdayFrom.Size = new System.Drawing.Size(55, 20);
            this.timeWednesdayFrom.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Onsdag";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(121, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "til";
            // 
            // timeTuesdayTo
            // 
            this.timeTuesdayTo.CustomFormat = "HH:mm";
            this.timeTuesdayTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeTuesdayTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeTuesdayTo.Location = new System.Drawing.Point(141, 48);
            this.timeTuesdayTo.Name = "timeTuesdayTo";
            this.timeTuesdayTo.ShowUpDown = true;
            this.timeTuesdayTo.Size = new System.Drawing.Size(55, 20);
            this.timeTuesdayTo.TabIndex = 6;
            // 
            // timeTuesdayFrom
            // 
            this.timeTuesdayFrom.CustomFormat = "HH:mm";
            this.timeTuesdayFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeTuesdayFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeTuesdayFrom.Location = new System.Drawing.Point(60, 48);
            this.timeTuesdayFrom.Name = "timeTuesdayFrom";
            this.timeTuesdayFrom.ShowUpDown = true;
            this.timeTuesdayFrom.Size = new System.Drawing.Size(55, 20);
            this.timeTuesdayFrom.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tirsdag";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(121, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "til";
            // 
            // timeMondayTo
            // 
            this.timeMondayTo.CustomFormat = "HH:mm";
            this.timeMondayTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeMondayTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeMondayTo.Location = new System.Drawing.Point(141, 22);
            this.timeMondayTo.Name = "timeMondayTo";
            this.timeMondayTo.ShowUpDown = true;
            this.timeMondayTo.Size = new System.Drawing.Size(55, 20);
            this.timeMondayTo.TabIndex = 2;
            // 
            // timeMondayFrom
            // 
            this.timeMondayFrom.CustomFormat = "HH:mm";
            this.timeMondayFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeMondayFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeMondayFrom.Location = new System.Drawing.Point(60, 22);
            this.timeMondayFrom.Name = "timeMondayFrom";
            this.timeMondayFrom.ShowUpDown = true;
            this.timeMondayFrom.Size = new System.Drawing.Size(55, 20);
            this.timeMondayFrom.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mandag";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbHour);
            this.groupBox2.Controls.Add(this.rbHalfHour);
            this.groupBox2.Controls.Add(this.rbQuarter);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(229, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(211, 91);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ugekalender Opløsning";
            // 
            // rbHour
            // 
            this.rbHour.AutoSize = true;
            this.rbHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbHour.Location = new System.Drawing.Point(7, 67);
            this.rbHour.Name = "rbHour";
            this.rbHour.Size = new System.Drawing.Size(72, 17);
            this.rbHour.TabIndex = 2;
            this.rbHour.TabStop = true;
            this.rbHour.Text = "Hele timer";
            this.rbHour.UseVisualStyleBackColor = true;
            // 
            // rbHalfHour
            // 
            this.rbHalfHour.AutoSize = true;
            this.rbHalfHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbHalfHour.Location = new System.Drawing.Point(7, 43);
            this.rbHalfHour.Name = "rbHalfHour";
            this.rbHalfHour.Size = new System.Drawing.Size(78, 17);
            this.rbHalfHour.TabIndex = 1;
            this.rbHalfHour.TabStop = true;
            this.rbHalfHour.Text = "Halve timer";
            this.rbHalfHour.UseVisualStyleBackColor = true;
            // 
            // rbQuarter
            // 
            this.rbQuarter.AutoSize = true;
            this.rbQuarter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbQuarter.Location = new System.Drawing.Point(7, 20);
            this.rbQuarter.Name = "rbQuarter";
            this.rbQuarter.Size = new System.Drawing.Size(59, 17);
            this.rbQuarter.TabIndex = 0;
            this.rbQuarter.TabStop = true;
            this.rbQuarter.Text = "Kvarter";
            this.rbQuarter.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dateTimePicker1);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(229, 109);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(211, 113);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Øvrigt";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "HH:mm";
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(102, 19);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowUpDown = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(56, 20);
            this.dateTimePicker1.TabIndex = 29;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(9, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 13);
            this.label16.TabIndex = 28;
            this.label16.Text = "Global Cooldown";
            // 
            // WeekPlannerSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 238);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "WeekPlannerSettings";
            this.Text = "Indstillinger for ugeplanlægger";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker timeSundayTo;
        private System.Windows.Forms.DateTimePicker timeSundayFrom;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker timeSaturdayTo;
        private System.Windows.Forms.DateTimePicker timeSaturdayFrom;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker timeFridayTo;
        private System.Windows.Forms.DateTimePicker timeFridayFrom;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker timeThursdayTo;
        private System.Windows.Forms.DateTimePicker timeThursdayFrom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker timeWednesdayTo;
        private System.Windows.Forms.DateTimePicker timeWednesdayFrom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker timeTuesdayTo;
        private System.Windows.Forms.DateTimePicker timeTuesdayFrom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker timeMondayTo;
        private System.Windows.Forms.DateTimePicker timeMondayFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbHour;
        private System.Windows.Forms.RadioButton rbHalfHour;
        private System.Windows.Forms.RadioButton rbQuarter;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label16;
    }
}